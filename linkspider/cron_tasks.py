#from celery import Celery
#from subprocess import call


import sys, os
import datetime
import pytz
from django.utils import timezone

sys.path.append("/home/mh/MediaMonitor")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MediaMonitor.settings")
from monitor import models

from statsspider import spider

#app = Celery('tasks', broker='amqp://guest@localhost//')
#app.config_from_object("celeryconfig")

def crawl():
    call_agora_crawler()
    call_onet_crawler()


def get_and_update_stats():
    get_stats()
    populate_stats() 

def call_agora_crawler():
    call(["scrapy", "crawl", "agora"])

def call_onet_crawler():
    call(["scrapy", "crawl", "onet"])


def get_stats():
    spider.get_facebook_and_twitter_stats()


def populate_stats():
    time_limit = timezone.now() - timezone.timedelta(days=31)
    all_links = models.Link.objects.filter(ttl__gt=time_limit)
    for link in all_links:
        link.populate_og_stats()


def populate_stats2():
    time_limit = timezone.now() - timezone.timedelta(days=31)
    all_links = models.Link.objects.filter(ttl__gt=time_limit)
    for link in all_links:
        link.populate_og_stats2()
        
        
if __name__ == '__main__':
    populate_stats2()



