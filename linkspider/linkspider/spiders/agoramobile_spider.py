import scrapy

from collections import defaultdict
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor


from linkspider.items import AgoraItem

import ipdb
from agora_spider import AgoraSpider

class AgoraMobileSpider(AgoraSpider):
    """
    This class inhertis methods from scrapy.contrib.spider.CrawlSpider and is a scrapy spider
    allowed_domains: a list of domains on wich crawling is allowed
    start_urls: a list of urls where to start scrapping
    """
    name = "agoramobile"
    allowed_domains = ["gazeta.pl", "wyborcza.pl", "wyborcza.biz", "plotek.pl", "sport.pl", "moto.pl", "lula.pl", "edziecko.pl", "deser.pl", \
                       "gazetapraca.pl", "gazetadom.pl", "domiporta.pl", "alert24.pl", "gazeta.tv", "agora.pl", "searchlab.pl", "tokfm.pl", "tuba.pl"]
    start_urls = ["http://m.gazeta.pl/","http://m.wyborcza.pl", "http://m.wyborcza.biz"]

#    xpaths = '''//a/@href'''
    rules = ( Rule(LinkExtractor(allow_domains = allowed_domains ) , callback="parse", follow=True), )
