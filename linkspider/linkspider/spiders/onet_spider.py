import scrapy
import ipdb
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

from collections import defaultdict
from linkspider.items import OnetItem


class OnetSpider(CrawlSpider):
    """
    This class inhertis methods from scrapy.contrib.spider.CrawlSpider and is a scrapy spider
    allowed_domains: a list of domains on wich crawling is allowed
    start_urls: a list of urls where to start scrapping
    """

    name = "onet"
    allowed_domains = ["onet.pl"]
    start_urls = ["http://www.onet.pl", "http://m.onet.pl"]

#    xpaths = '''//a/@href'''
    rules = ( Rule(LinkExtractor(allow_domains = allowed_domains ) , callback="parse", follow=True), )
    def parse(self, response):
        """
        This function takes HTTPresponse object and extracts following info:
        1. url  - target address
        2. section_name - id given to this link. On GAZETA.pl site id is used in a strange way (as class should be used, i.e in the non-unique way)
        3. link_position_within - link position in its section
        4. nr - number of the url as appeared in html source code
        5. source_url - url of the site with this link
        This function returns linkspider.items.OnetItem instance.
        """
        articles = response.xpath("//article")
        urls_position = defaultdict(list) # link_positions_within_id
        nr = 0
        for article in articles:
            nr += 1
            article_datasection_name = article.xpath('@data-section').extract()
            section_datasection_name = article.xpath('.//section/@data-section').extract()
            multiple_sections = False

            if len(article_datasection_name) > 0:
                section_name = article_datasection_name[0]
                print "<article @data-section " + article_datasection_name[0] + "###"
            elif len(section_datasection_name) > 0:
                if len(section_datasection_name) > 1:
                    section_names = section_datasection_name
                    multiple_sections = True
                section_name = section_datasection_name[0]
                print "<section " + section_datasection_name[0] + "###"
            else:
                section_name = "UNKNOWN"

            

            if multiple_sections:
                #ipdb.set_trace()
                sections = article.xpath('.//section')
                sections_urls = [ ( section.xpath(".//a").xpath("@href").re(r'^http.*'), section.xpath('@data-section').extract()[0] ) for section in sections ]
            else:
                urls_in_section = article.xpath(".//a").xpath("@href").re(r'^http.*')
                sections_urls = [ (urls_in_section, section_name),]
            
            for urls_in_section, section_name in sections_urls:
                for url in urls_in_section:
                    item = OnetItem()
                    #ipdb.set_trace()
                    urls_position[section_name].append(url)
                    #print url,
                    #print " -- !! SECTION " + section_name
                    item['url'] = url
                    item['link_number_on_site'] = nr
                    item['link_id'] = section_name
                    item['source_url'] = response.url
                    item['link_position_within'] = len( urls_position[ section_name ] ) # our item will always be last
                    assert urls_position[section_name][(item['link_position_within']-1)] == url #but let's test that assumption	
                    yield item
                    
                
                