from django.shortcuts import get_object_or_404, render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseNotFound
from django.core.exceptions import MultipleObjectsReturned

import models
import json_helpers
import ipdb

import django_tables2 as tables
from django_tables2   import RequestConfig
from django_tables2.utils import A

class IndexTable(tables.Table):
    title_url = tables.TemplateColumn('<a href="{{record.uri}}">{{record.title}}</a>', verbose_name="title")
    shares_count=  tables.TemplateColumn('{{record.last_shares_count}}<a href="/monitor/linkstats/{{record.id}}">(show all)</a>', verbose_name="Last shares count")
    link_position_history = tables.TemplateColumn('<a href="/monitor/linkpositions/{{record.id}}">(show all)</a>')
    
    class Meta:
        model = models.Link
        attrs = {'class' : 'table table-bordered table-striped table-condensed'}
        exclude = ("id", "uri", "ttl", "stats", "link_type", "created_at", "updated_at", "title", "last_shares_count")
        sequence  = ("title_url", "source_url", "shares_count", "last_comments_count", "link_number_on_site", "link_id", "link_position_within", "...")

def index(request):
    """
    Render an index of all scrapped articles. Show only those entities that
    have open graph property 'type' set to 'article'. Requires process
    linkspider/populate_stats to complete, only then we are able to
    distinguish between different types of articles - thanks to
    open graph tag "type" collected from facebook. We could parse the
    article itself, however it is not opened by the scrapper to save
    bandwidth.
    """

    #link_list = models.Link.objects.all()

    page = request.GET.get('page')
    articles =  models.Link.objects.filter(link_type = 'article').order_by('-created_at');
    """
    paginator = Paginator(articles, 200)
    #ipdb.set_trace()
    try:
        index_entries_paged = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        index_entries_paged = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        index_entries_paged = paginator.page(paginator.num_pages)
    """
    table = IndexTable(articles)
    RequestConfig(request).configure(table)
    return render(request, "links/index.html", {"table": table})
    
    #return render(request, "links/index.html", {'index' : index_entries_paged})

def get_positions(request, link_id):
    link = get_object_or_404(models.Link, pk=link_id)
    if request.META.has_key('HTTP_REFERER'):
        referer = request.META['HTTP_REFERER']
    else:
        referer = "#"
    positions = link.linkposition_set.all()
    return render(request, "positions/show.html", {
                                                    'uri' : link.uri,
                                                    'HTTP_REFERER': referer ,
                                                    'positions' : positions,
                                                    'updated_time' : link.updated_time ,
                                                    'created_at' : link.created_at
                                                    })


def get_stats(request, link_id):
    link = get_object_or_404(models.Link, pk=link_id)
    return render_stats(request, link)
    
def render_stats(request, link):
    if request.META.has_key('HTTP_REFERER'):
        referer = request.META['HTTP_REFERER']
    else:
        referer = "#"
    stats = json_helpers.get_stats(link)
    #ipdb.set_trace()
    return render(request, "stats/show.html", {'uri' : link.uri,
                                               'HTTP_REFERER': referer ,
                                               'updated_time' : link.updated_time ,
                                               'created_at' : link.created_at,
                                               'col_names' : stats[0].keys(),
                                               'stats' : stats})


def get_stats_by_uri(request, uri):
    #ipdb.set_trace()
    try:
        link = get_object_or_404(models.Link, uri__iregex=uri)
    except MultipleObjectsReturned:
        return HttpResponseNotFound('<h1>There is more than one url that match this query.</h1>')
    
    return render_stats(request, link)
    
    
    
    
    
    