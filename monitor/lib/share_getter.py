import json
import ipdb
from lxml import etree
import urllib
import urllib2
from time import time, sleep
import datetime
import facebook
from django.utils import timezone
import pytz
from mysettings import FACEBOOK_APP_ID, FACEBOOK_APP_SECRET
from multiprocessing import Process, Queue


class StatsGetter(object):
    def __init__(self, linklist, lag=1, query_timeout=10):
        """
        linklist_file : file/buffer/list with list of links to check separated by newline
        """
        assert type(linklist) in (file, buffer, list), "Excepted file, buffer or list"
        self.__configure_time()
        self.__setup_fb()
        self.TWITTER_API = 'http://cdn.api.twitter.com/1/urls/count.json?url=%s'
        self.GRAPH_API = "https://graph.facebook.com"
        self.query_timeout = query_timeout
        #self.TWITTER_API = 'https://cdn.api.twitter.com/1/urls/count.json?url='
        self.link_count = 0
        self.lag = lag
        self.linklist = linklist
        self.stats = self.get_stats_for_links()
        

    
    def __configure_time(self):
        self.tzname = "Europe/Warsaw"
        timezone.activate(pytz.timezone(self.tzname))
        self.tz  = pytz.timezone("Europe/Warsaw")

    def get_time(self):
        return datetime.datetime.now().replace(tzinfo=self.tz)

    def __setup_fb(self):
        self.fb_app_token = facebook.get_app_access_token(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET)
        self.graph = facebook.GraphAPI(self.fb_app_token, version="2.1")
        self.rest_url = "http://api.facebook.com/restserver.php?method=links.getStats&urls="

    def to_json(self, query):
        return json.loads(urllib2.build_opener().open(query, timeout=self.query_timeout).read())

    def get_rest_url(self):
        return self.rest_url

    def make_fb_api21_batch_request(self, lst, batch_size = 40):
        objects = []
        
        counter = 0
        error_count = 0
        for sublst in self.list_partition(lst,len(lst)/batch_size):
            
            objects = []
            
            for uri in sublst:
                try:
                    uri.encode('utf-8')
                    objects.append({"method":"GET", "relative_url": uri })
                    counter += 1
                except UnicodeDecodeError:
                    error_count +=1
                    print "Error parsing url. Ignoring this for the moment. %d th error" % (error_count)
            start = time()
            response = self.fb_url_batch_req(objects)
            print "Processed %d objects" % counter
            if time() - start < 1:
                sleep(time() - start)
            yield response
        
        """
        for i, link in enumerate(lst):
            
            print "link %d : %s" % (i,link)
            if counter < limit-1 and i < len(lst) - 1:
                counter += 1
                objects.append({"method":"GET", "relative_url": link } )
            elif i == len(lst) - 1: # last iteration
                objects.append({"method":"GET", "relative_url": link } )
                #ipdb.set_trace()
                yield self.fb_url_batch_req(objects)
                sleep(self.lag)
            else:
                counter = 0
                objects.append({"method":"GET", "relative_url": link } )
                #ipdb.set_trace()
                yield self.fb_url_batch_req(objects)
                sleep(self.lag)
        """
    def fb_url_batch_req(self, objects):
        values = {}
        values['access_token'] = self.fb_app_token
        values['batch'] = objects
        data= urllib.urlencode(values)
        #ipdb.set_trace()
        req = urllib2.Request(self.GRAPH_API, data)
        try:
            response = urllib2.urlopen(req)
        except urllib2.HTTPError:
            ipdb.set_trace()
        json_obj = response.read()
        return json.loads(json_obj)
        

    def get_fb_api21_stats(self, lst, que):
        for i,link in enumerate(lst):
            if link == "":
                que.put( { 'url': link, 'json' : None, 'time' : self.get_time() } )
            #response = urllib.urlopen(self.FB_API + link)
            #ipdb.set_trace()
            else:
                #if i % 50 == 0:
                #    print "FB %d" % (i)
                #    sleep(self.lag)
                try:
                    que.put( { 'url': link, 'json' : self.graph.get_object(link), 'time' : self.get_time() } ) 
                except facebook.GraphAPIError:
                    que.put( { 'url': link, 'json' : None, 'time' : self.get_time() } )
                except UnicodeError:
                    que.put( { 'url': link, 'json' : None, 'time' : self.get_time()  } )
                print "Got %dth link: %s" % (i, link)
                sleep(self.lag)
                
    def get_fb_rest_stats(self, link):
        raise NotImplementedError
        """
        if self.link_count % 50 == 0:
            sleep(self.lag)
        try:
            response = urllib.urlopen( self.get_rest_url() + link )
        except UnicodeError:
            return {}
        tree = etree.parse(response)
        root = tree.getroot()
        link_stat = root.getchildren()[0]
        stats_dict = dict()
        for child in link_stat.getchildren():
            stats_dict[ child.tag ] = child.text
        return stats_dict
        """
   
    def get_twitter_stats(self, que):
        for i,link in enumerate(self.linklist):
            if link == "":
                que.put( { 'url': link, 'json' : None } )
                continue
            else:
                print "TW %d" % (i)
                #if i % 50 == 0:
                #    sleep(self.lag)
                try:
                    print link
                    query = self.TWITTER_API % urllib.quote(link)
                    #response = urllib.urlopen(self.TWITTER_API + link)
                    json_obj = self.to_json(query)
                except UnicodeError:
                    que.put( { 'url': link, 'json' : None, 'time' : self.get_time()  } )
                    continue
                except ValueError:
                    que.put( { 'url': link, 'json' : None, 'time' : self.get_time()  } )
                    continue
                # IOError : mostly timeouts, just skip the link, wait 10 sec, try
                # again, than give up
                except IOError: # TIMEOUT occured
                    sleep(self.lag)
                    try:
                        query = self.TWITTER_API % urllib.quote(link)
                        print "QUERY:", query
                        json_obj = self.to_json(query)
                    except IOError:
                        que.put( { 'url': link, 'json' : None, 'time' : self.get_time()  } ) # giving up, return empty json
                        continue
                    except ValueError:
                        que.put( { 'url': link, 'json' : None, 'time' : self.get_time()  } )
                        continue
                que.put( { 'url': link, 'json' : json_obj, 'time' : self.get_time() } )
                print "Got %s" % link
                sleep(self.lag)
    """
    def get_link(self, link):
        self.link_count +=1
        if link == "":
            # Don't pass empty 'links' to APIs, they generate errors 
            return {'fb_21' : {}, 'fb_rest': {}, 'twitter' : {}, 'time' : datetime.datetime.now().replace(tzinfo=self.tz) }
        fb_21_response = self.get_fb_api21_stats(link)
        fb_REST_response = {} #self.get_fb_rest_stats(link)
        tw_response = self.get_twitter_stats(link)
        return {'fb_21' : fb_21_response, 'fb_rest': fb_REST_response, 'twitter' : tw_response, 'time' : datetime.datetime.now().replace(tzinfo=self.tz) }
    """
    def list_partition(self, lst, n):
        if n > 0:
            q, r = divmod(len(lst), n)
            indices = [q*i + min(i, r) for i in xrange(n+1)]
            return [lst[indices[i]:indices[i+1]] for i in xrange(n)]
        else:
            return [lst]
    """
    def list_partition(self, lst, n):
        return [ lst[i::n] for i in xrange(n) ] # partition into 20 groups
        
    """
    def get_stats_for_links(self,nproc = 1):
        #fb_21_stats = self.get_facebook_stats()
        #twitter_stats = self.get_twitter_stats()
        """
       fb_queue = Queue()

        lists = self.list_partition(self.linklist, nproc)
        ps = []
        for lst in lists:
            fb_process = Process(target = self.get_fb_api21_stats, args=(lst, fb_queue, ))
            fb_process.start()
            ps.append(fb_process)

        for process in ps:
            process.join()
        """
        #tw_queue = Queue()
        #tw_process = Process(target = self.get_twitter_stats,  args=( tw_queue, ))
        #tw_process.start()
        
        response_set = self.make_fb_api21_batch_request(self.linklist)
        
        def unpack_results(response_set):
            #res = [batch for batch in response_set]
            res = []
            for batch_result in response_set:
                for row in batch_result:
                    res.append(row['body'])
            return res
        
        results  = unpack_results(response_set)
        
        
        #results = { 'fb_21': fb_queue }#, 'twitter': tw_queue }
        #tw_process.join()
        #fb_process.join()
        #ipdb.set_trace()
        
        return results
        #return [self.get_link(link) for link in self.linklist]
        #return self.get_sharedcount(self.linklist)

    def get_stats(self):
        return self.stats



if __name__ == '__main__':
    #check some links
    """
    l = ["http://wiadomosci.gazeta.pl/wiadomosci/1,114871,17354409,Kaminski_o_zmianach_w_otoczeniu_Kopacz__Nie_jestesmy.html#BoxNewsLink"
     , "http://podroze.onet.pl/ciekawe/festiwal-sahary-w-douz-w-tunezji-atrakcje-informacje/srqrc"
     , "http://wyborcza.biz/biznes/0,104259.html?tag=g%F3rnictwo"
     , "http://wyborcza.pl/1,75477,17345800,Splonely_dwa_miliony_ksiazek___to_przypomina_Czarnobyl__.html"
     , "http://samcik.blox.pl/2015/01/Klient-wpisal-poprawke-do-umowy-kredytowej-W.html"
     , "http://kobieta.gazeta.pl/kobieta/56,142401,14999593,Zimowa_pielegnacja__Na_co_zwrocic_szczegolna_uwage.html#BoxLSLink"
     , "http://wyborcza.pl/1,75478,17367119,Gotowy_protokol_uzgodnien_i_rozbieznosci_w_JSW.html"
     , "http://bialystok.gazeta.pl/bialystok/1,35235,17359842,Szef_browaru_Dojlidy_z_zarzutami__Za_zburzenie_zabytku.html#BoxLokKrajLink"
     , "http://podroze.gazeta.pl/podroze/1,114158,8907540,Turystyka_zakupowa__Zimowe_wyprzedaze___Paryz__Londyn_,,ga.html#TROgl"
     , "http://wyborcza.pl/1,75477,17366621,Doping_w_Rosji__Czy_trenerzy_dzieci_dostana_linijka.html#BoxGWImg"
     , "http://wroclaw.domiporta.pl/nieruchomosci/sprzedam-mieszkanie-wroclaw-krzyki-ok-ronda-powstancow-slaskich-43m2/137225879"
     , "http://wyborcza.pl/1,75478,17332277,Po_zmniejszeniu_chorobowego_mundurowi_przestali_chorowac.html"
     , "http://www.plotek.pl/plotek/56,79592,17363460,Kate_i_William_z_synem_Jerzym_wyjechali_na_egzotyczne.html#Prze"
     , "http://www.krakow.sport.pl/sport-krakow/1,115698,17318027,Marko_Jovanovic_moze_juz_nie_zagrac__ale_i_tak_mial.html#BoxSportLink"
     , "http://wyborcza.pl/1,75478,17330169,Audi__ktore_stoczylo_sie_do_Wisly__zniknelo_jak_kamfora.html"
     , "http://magazyn-kuchnia.pl/magazyn-kuchnia/56,123978,15398495,Cokolwiek__byle_w_panierce_.html#BoxLSLink"
     , "http://www.komunikaty.pl/komunikaty/0,79738.html?xx_announ=5365965"
     , "http://www.sport.pl/reczna/12,78466,17332837,MS_w_Katarze__Najlepsze_podsumowanie_wygranej_z_Chorwacja_.html#BoxVidImg"
     , "http://film.onet.pl/wkreceni-2-lukasz-zagrobelny-spiewa-dla-barbary-kurdej-szatan/z96zg"
     , "http://wiadomosci.onet.pl/lodz/lodz-wybuch-gazu-w-budynku-mieszkalnym/ssqxr"
     , "http://wyborcza.biz/biznes/1,101716,17308763,Spisek_w_ropie_i_kres_rajskich_czasow_dla_kierowcow_.html"
     , "http://eurosport.onet.pl/pilka-reczna/mistrzostwa-swiata-pilkarzy-recznych/francja-argentyna-trojkolorowi-spacerkiem-weszli-do-cwiercfinalu-ms/we7ml"
     , "http://wiadomosci.gazeta.pl/wiadomosci/1,114871,17332760,Barbarzynstwo_terrorystow_z_IS__Wysadzili_w_powietrze.html#BoxNewsLink"
     , "http://wyborcza.biz/biznes/10,100970,17368699,Umowa_na_ekspresowke_Olsztyn___Olsztynek__.html"
     , "http://warszawa.domiporta.pl/nieruchomosci/sprzedam-mieszkanie-warszawa-mokotow-kmicica-80m2/137306477"]
    """    
    l = [
        u'http://weekend.gazeta.pl/weekend/1,138585,17298735,Technikum_w_liceum__czyli_Bednarska_Szkola_Realna_.html#TRwknd',
         u'http://weekend.gazeta.pl/weekend/1,138262,17261647,Potrafi_zapamietac_ciag_stu_cyfr_w_30_sekund__ma_dwanascie.html#TRwknd',
        u'http://weekend.gazeta.pl/weekend/1,138589,17294555,Pigulka__dzien_po_____Nie_wiem_jak_dziala__ale_jestem.html#TRwknd',
        u'http://weekend.gazeta.pl/weekend/1,140251,17295292,Ilona_Lepkowska__Praca_przy_serialach__Placa_za_to.html#TRwknd',
        u'http://weekend.gazeta.pl/weekend/1,138262,17299314,Tworcy_projektu___Tatuaze_wolnosci____Chcemy__aby.html#TRwknd',
        u'http://weekend.gazeta.pl/weekend/1,138262,17289623,Irena_Eris__Nie_znalazlam_miejsca__gdzie_moglabym.html#TRwknd',
        u'http://weekend.gazeta.pl/weekend/1,138262,17278176,Monica_Serrano__konna_toreadorka__Kazdego_popoludnia.html#TRwknd',
        u'http://weekend.gazeta.pl/weekend/1,140251,17261294,Lukasz_Barczyk__Krew_i_honor__Piekne_hasla__ale_nie.html#TRwknd',
        u'http://weekend.gazeta.pl/weekend/1,138262,17293122,__Skonczylo_sie_zycie__a_wies_przestala_istniec___.html#TRwknd',
        u'http://poczta.gazeta.pl/#NavLinks'
         ]

    sg = StatsGetter([x.encode('utf-8') for x in l],lag=1, query_timeout=10)
    print sg.stats

