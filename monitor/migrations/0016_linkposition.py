# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitor', '0015_auto_20150219_2327'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkPosition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(db_index=True)),
                ('link_position_within', models.IntegerField(null=True)),
                ('link_html_id', models.CharField(max_length=1000, null=True)),
                ('link_number_on_site', models.IntegerField(null=True)),
                ('link', models.ForeignKey(to='monitor.Link')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
