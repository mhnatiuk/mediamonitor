# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('monitor', '0013_auto_20141031_1459'),
    ]

    operations = [
        migrations.RenameField(
            model_name='link',
            old_name='position',
            new_name='link_number_on_site',
        ),
        migrations.RemoveField(
            model_name='link',
            name='domain',
        ),
        migrations.AddField(
            model_name='link',
            name='added_time',
            field=models.DateTimeField(default=None, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='link',
            name='link_id',
            field=models.CharField(max_length=1000, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='link',
            name='link_position_within',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='link',
            name='source_url',
            field=models.CharField(max_length=1000, null=True),
            preserve_default=True,
        ),
    ]
