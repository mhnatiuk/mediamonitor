from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^linkstats/(?P<link_id>\d+)$', views.get_stats, name='get_stats'),
    # dispatch http:// links as checking statistics for this url
    url(r'^(http:\/\/.*)#?.*', views.get_stats_by_uri, name='get_stats_by_uri'),
    url(r'^linkpositions/(?P<link_id>\d+)$', views.get_positions, name='get_positions'),
)