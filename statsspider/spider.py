import sys
#import ipdb
import os
import ipdb
sys.path.append("/home/m/MediaMonitor")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MediaMonitor.settings")
from collections import defaultdict
from monitor.lib import share_getter
from monitor.models import Link, LinkStats
from django.utils import timezone
from django.db.models import Q
import json
import django
django.setup()
import datetime
import pytz
from django.utils import timezone

from Queue import Empty
from Queue import Queue
#import datetime


def get_all_valid_links():
    """
    Get all links with valid Time To Live value
    """
    # only those that are in some box
    all_links = Link.objects.filter(~Q(link_id = 'UNKNOWN') )
    valid_links = [ link for link in all_links if link.ttl > timezone.now() ]
    return valid_links

def get_facebook_and_twitter_stats():
    """
    For all valid links get statistics from Facebook and
    Twitter APIs and save them as JSON into DB
    """
    django.setup()
    tzname = "Europe/Warsaw"
    timezone.activate(pytz.timezone(tzname))
    tz  = pytz.timezone("Europe/Warsaw")

    
    json_en = json.JSONEncoder()
    links = get_all_valid_links()
    stats_getter = share_getter.StatsGetter([link.uri.encode('utf-8') for link in links], lag=1, query_timeout = 10)

    
    stats  = stats_getter.get_stats()
    stats_obj = {}
    
    json_de = json.JSONDecoder()
    for stat in stats:
        json_obj = json_de.decode(stat)
        try:
            stats_obj[ json_obj['id'] ] = json_obj
        except KeyError:
            pass
        
    #ipdb.set_trace()
    #url_stats = defaultdict(lambda: defaultdict(str))
    #ipdb.set_trace()
    """
    while True:
        try:
            twitter_obj = stats['twitter'].get(0)
            url_stats[ twitter_obj['url'] ]['twitter'] = twitter_obj['json']
        except Empty:
            break
    """
    """
    while True:
        try:
            fb_obj = stats['fb_21'].get(0)
            url_stats[ fb_obj['url'] ]['fb_21'] = fb_obj['json']
            url_stats[ fb_obj['url'] ]['time'] = fb_obj['time']
            #url_stats[fb_obj['url']] = fb_obj['json']
        except Empty:
            break
    """
    
    for i, link in enumerate(links):
        ls = LinkStats()
        ls.link_id = link.id
        ls.link_uri = link.uri
        #ipdb.set_trace()
        if stats_obj.has_key(link.uri):
            ls.fb_rest = '{}' #json_en.encode(stats[i]['fb_rest'])
            """
            if url_stats[link.uri]['fb_21'] is not None:
                ls.fb_21 = json_en.encode(url_stats[link.uri]['fb_21'])
            if url_stats[link.uri]['time'] is not None:
                ls.time = url_stats[link.uri]['time']
            if url_stats[link.uri].has_key('twitter') and url_stats[link.uri]['twitter'] is not None:
                ls.twitter = json_en.encode(url_stats[link.uri]['twitter'])
            """
            ls.twitter = '{}'
            ls.fb_21 = json_en.encode( stats_obj[link.uri] )
            ls.time = datetime.datetime.now().replace(tzinfo=tz)
            ls.save()
        else:
            pass # dont bother to save empty stats


if __name__ == "__main__":
    get_facebook_and_twitter_stats()
