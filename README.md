# MediaMonitor
Monitoring tool for Polish news sites articles popularity in social media. This work was supported by the National Science Centre ([Narodowe Centrum Nauki](https://www.ncn.gov.pl/)) under Grant 2012/05/B/HS6/03802.

Main packages used (see below for details pip freeze output):
 
* django
* scrapy
* pytz
* facebook-sdk, install: pip install  git+git://github.com/pythonforfacebook/facebook-sdk@master
  
Roadmap:
  
          MediaMonitor/ : django project folder with all core settings for django
          
          linkspider/ : tools for scrapping links from sites
          
            linkspider/ : scrapy application for crawling Agora and Onet
            
              items.py : defines what information from each link is to be processed further
              
              pipelines.py : defines functions to process those items
              
              settings.py : scrapy settings: set log level, user agent and pipelines (see above)
              
              spiders/
              
                agora_spider.py : crawler defintion
                
                onet_spider.py : crawler defintion
                
          monitor/ : django app: contains :
          
            views.py -  how to display the data
            
            models.py - Database models of links and stats: writing, reading, updating links and stats
            
            lib/share_getter.py - a core library for getting sharing statistics from FB and TW
            
            templates/ - templates for displaying the data
            
          statspider/ : spider for scrapping popularity stats from social media sites
          
            spider.py: core script for getting the data (using monitor/lib/share_getter.py)
REQUIREMENTS
---------------------------

* Django==1.7.4
* Scrapy==0.24.4
* Twisted==14.0.2
* argparse==1.2.1
* cffi==0.8.6
* cryptography==0.7.2
* cssselect==0.9.1
* distribute==0.6.24
* django-model-utils==2.2
* django-pandas==0.2.2
* django-tables2==0.15.0
* enum34==1.0.4
* facebook-sdk==1.0.0-alpha
* facepy==1.0.6
* lxml==3.4.1
* numpy==1.9.2
* pandas==0.16.0
* psutil==2.2.1
* psycopg2==2.5.4
* pyOpenSSL==0.14
* pyasn1==0.1.7
* pycparser==2.10
* pycurl==7.19.5.1
* pyfb==0.4.2
* python-dateutil==2.4.2
* pytz==2014.10
* queuelib==1.2.2
* requests==2.5.1
* simplejson==3.6.5
* six==1.9.0
* w3lib==1.11.0
* wsgiref==0.1.2
* zope.interface==4.1.2